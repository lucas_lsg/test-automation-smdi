/// <reference types="cypress" />
import Navigation_Controller from '../Controller/Navigation_Controller.js';
import Action_Controller from '../Controller/Action_Controller.js';
import SignGoogle_Controller from '../Controller/SignGoogle_Controller.js';

describe(`
    Given I'm Home page
    When I'm Home to Action Page
    Then I should see all elements from Action page
    And crud should be working correctly`,()=>{

    let navigation_Controller = new Navigation_Controller;
    let action_Controller = new Action_Controller;
    let signGoogle_Controller = new SignGoogle_Controller;

    before('Open Home page',()=>{
        signGoogle_Controller.signGooglePage()
    })

    it('Checking CRUD from Action Page',()=>{
        var name_test = 'ACTION TEST'
        navigation_Controller.goToElementsAction()
        action_Controller.createAction(name_test)
        action_Controller.editAction(name_test)
        action_Controller.deleteAction(name_test)

    })

})