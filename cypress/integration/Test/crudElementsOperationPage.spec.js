/// <reference types="cypress" />
import Navigation_Controller from '../Controller/Navigation_Controller.js';
import Operation_Controller from '../Controller/Operation_Controller.js';
import SignGoogle_Controller from '../Controller/SignGoogle_Controller.js';

describe(`
    Given I'm Home page
    When I'm Home to Operation Page
    Then I should see all elements from operation page
    And crud should be working correctly`,()=>{

    let navigation_Controller = new Navigation_Controller;
    let operation_Controller = new Operation_Controller;
    let signGoogle_Controller = new SignGoogle_Controller;

    before('Open Home page',()=>{
        signGoogle_Controller.signGooglePage()
    })

    it('Checking CRUD from Operation Page',()=>{
        navigation_Controller.goToElementsOperation()
        operation_Controller.createOperation('ASSEMBLE CAMERA', 'ASSEMBLE CAMERA LENS', 'CAMERA : 2 : 3.50', 'OPERATION TEST')
        operation_Controller.editOperation('OPERATION TEST')
        operation_Controller.deleteOperation('OPERATION TEST')

    })

})