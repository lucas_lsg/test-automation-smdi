/// <reference types="cypress" />
import Navigation_Controller from '../Controller/Navigation_Controller.js';
import Product_Controller from '../Controller/Product_Controller.js';
import SignGoogle_Controller from '../Controller/SignGoogle_Controller.js';

describe(`
    Given I'm Home page
    When I'm Home to Product Page
    Then I should see all elements from product page
    And crud should be working correctly`,()=>{

    let navigation_Controller = new Navigation_Controller;
    let product_Controller = new Product_Controller;
    let signGoogle_Controller = new SignGoogle_Controller;

    before('Open Home page',()=>{
        signGoogle_Controller.signGooglePage()
    })

    it('Checking CRUD from Product Page',()=>{
        navigation_Controller.goToElementsProduct()
        product_Controller.editProduct('AFFINITY')

    })

})