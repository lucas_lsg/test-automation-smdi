/// <reference types="cypress" />
import Navigation_Controller from '../Controller/Navigation_Controller.js';
import Location_Controller from '../Controller/Location_Controller.js';
import SignGoogle_Controller from '../Controller/SignGoogle_Controller.js';

describe(`
    Given I'm Home page
    When I'm Home to Location Page
    Then I should see all elements from Location page
    And crud should be working correctly`,()=>{

    let navigation_Controller = new Navigation_Controller;
    let location_Controller = new Location_Controller;
    let signGoogle_Controller = new SignGoogle_Controller;

    before('Open Home page',()=>{
        signGoogle_Controller.signGooglePage()
    })

    it('Checking CRUD from Location Page',()=>{
        var name_test = 'LOCATION TEST'
        navigation_Controller.goToElementsLocation()
        location_Controller.createLocation(name_test)
        location_Controller.editLocation(name_test)
        location_Controller.deleteLocation(name_test)

    })

})