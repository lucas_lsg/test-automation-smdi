/// <reference types="cypress" />
import Navigation_Controller from '../Controller/Navigation_Controller.js';
import OperationType_Controller from '../Controller/OperationType_Controller.js';
import SignGoogle_Controller from '../Controller/SignGoogle_Controller.js';

describe(`
    Given I'm Home page
    When I'm Home to Operation type Page
    Then I should see all elements from operation type page
    And crud should be working correctly`,()=>{

    let navigation_Controller = new Navigation_Controller;
    let operationType_Controller = new OperationType_Controller;
    let signGoogle_Controller = new SignGoogle_Controller;

    before('Open Home page',()=>{
        signGoogle_Controller.signGooglePage()
    })

    it('Checking CRUD from Operation type Page',()=>{
        navigation_Controller.goToElementsOperationType()
        operationType_Controller.createOperationType('TYPE TEST', '1', 'DESCRIPTION TEST', '1')
        operationType_Controller.editOperationType('TYPE TEST')
        operationType_Controller.deleteOperationType('TYPE TEST')
    })

})