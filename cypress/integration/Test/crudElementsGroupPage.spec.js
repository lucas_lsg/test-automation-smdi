/// <reference types="cypress" />
import Navigation_Controller from '../Controller/Navigation_Controller.js';
import Group_Controller from '../Controller/Group_Controller.js';
import SignGoogle_Controller from '../Controller/SignGoogle_Controller.js';

describe(`
    Given I'm Home page
    When I'm Home to Group Page
    Then I should see all elements from group page
    And crud should be working correctly`,()=>{

    let navigation_Controller = new Navigation_Controller;
    let group_Controller = new Group_Controller;
    let signGoogle_Controller = new SignGoogle_Controller;

    before('Open Home page',()=>{
        signGoogle_Controller.signGooglePage()
    })

    it('Checking CRUD from Group Page',()=>{
        navigation_Controller.goToElementsGroup()
        group_Controller.createGroup('GROUP TEST NAME', 'APPLY THERMAL GEL')
        group_Controller.editGroup('GROUP TEST NAME')
        group_Controller.deleteGroup('GROUP TEST NAME')

    })

})