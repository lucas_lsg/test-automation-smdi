export default class ElementsLocationPage {

    getButtonNewLocation(){
        return cy.get('button[class="btn btn--primary"]',{timeout:10000}).contains('New Location')
    }
    getSearchLocation(){
        return cy.get('#name',{timeout:10000})
    }
    getLocationModalTitle(){
        return cy.get('.modal-title',{timeout:10000}).contains('Location Creation')
    } 
    getSelectNameModal(){
        return cy.get('#nameLocation',{timeout:10000})
    }
}