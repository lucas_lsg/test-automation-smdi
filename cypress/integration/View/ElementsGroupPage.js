export default class ElementsGroupPage {
   
    getButtonNewGroup(){
        return cy.get('button[class="btn btn--primary"]',{timeout:10000}).contains('New Group')
    }
    getInputNameModal(){
        return cy.get('#nameGroup',{timeout:10000})
    }
    getSelectOperationModal(){
        return cy.get('ng-select[bindlabel="description"]',{timeout:10000})
    }
    getSearchGroupPage(){
        return cy.get('#name',{timeout:10000})
    }
    getCheckBoxRealModal(){
        return cy.get('input[formcontrolname="real"]',{timeout:10000})
    }

    getButtonAddOperation(){
        return cy.get('button[class="btn btn--primary"]',{timeout:10000}).contains('Add Operation')
    }
    
}