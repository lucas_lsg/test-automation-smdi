export default class ElementsNavigationPage {
   
    getCard_NavigationPage(){
        return cy.get('.card__title',{timeout:60000}).contains('Apps').should('be.visible')
    }
    getCard_Simulation(){
        return cy.get('.applications > :nth-child(6)')
    }
    getButtonElements(){
        return cy.get('[routerlink="/main-panel/simulation/elements/"]')
    }
    getButtonOperationType(){
        return cy.get('[routerlink="/main-panel/simulation/elements/operation-type/"]')
    }
    getButtonLocation(){
        return cy.get('[routerlink="/main-panel/simulation/elements/location/"]')
    }
    getButtonAction(){
        return cy.get('[routerlink="/main-panel/simulation/elements/action/"]')
    }
    getButtonOperation(){
        return cy.get('[routerlink="/main-panel/simulation/elements/operation/"]')
    }
    getButtonProduct(){
        return cy.get('[routerlink="/main-panel/simulation/elements/product-simulation/"]')
    }
    getButtonGroup(){
        return cy.get('[routerlink="/main-panel/simulation/elements/group/"]')
    }
    getSearchElement(){
        return cy.get('#name')
    }
}