export default class ElementsActions {

    getHomeCardSignGoogle(){
        return cy.get('.home-card',{timeout:10000})
    }
    getButtonSignGoogle(){
        return cy.get('button[class="btn btn-form-login"]',{timeout:10000})
    }
    getButton_Save(){
        return cy.get('button[type="button"]',{timeout:10000}).contains('Save')
    }
    getButton_Cancel(){
        return cy.get('button[type="button"]',{timeout:10000}).contains('Cancel')
    }
    getFilterResult(){
        return cy.get('span[class="ng-option-label"]',{timeout:10000})   
    }
    getSearch(){
        return cy.get('#search',{timeout:10000})
    }
    getButtonEdit(){
        return cy.get('i[class="material-icons icon-delete-entity ml-3"]',{timeout:10000})
    }
    getButtonDelete(){
        return cy.get('i[class="material-icons icon-delete-entity"]',{timeout:10000})
    }
    getButton_Yes(){
        return cy.get('button[type="button"]',{timeout:10000}).contains('Yes')
    }
    getButton_No(){
        return cy.get('button[type="button"]',{timeout:10000}).contains('No')
    }
}
