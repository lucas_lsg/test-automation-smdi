export default class ElementsOperationTypePage {
   
    getButtonNewOperationType(){
        return cy.get('button[class="btn btn--primary"]',{timeout:10000}).contains('New Operation Type')
    }
    getInputTypeModal(){
        return cy.get('#nameType',{timeout:10000})
    }
    getInputComplexityModal(){
        return cy.get('#complexity',{timeout:10000})
    }
    getInputDescriptionModal(){
        return cy.get('#description',{timeout:10000})
    }
    getInputCTModal(){
        return cy.get('#cicloTime',{timeout:10000})
    }
}