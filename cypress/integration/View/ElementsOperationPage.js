export default class ElementsOperationPage {
   
    getButtonNewOperation(){
        return cy.get('button[class="btn btn--primary"]',{timeout:10000}).contains('New operation')
    }
    getSelectLocationModal(){
        return cy.get('ng-select[formcontrolname="location"]',{timeout:10000})
    }
    getSelectActionModal(){
        return cy.get('ng-select[formcontrolname="action"]',{timeout:10000})
    }
    getSelectTypeModal(){
        return cy.get('ng-select[formcontrolname="type"]',{timeout:10000})
    }
    getInputOperationModal(){
        return cy.get('input[formcontrolname="description"]',{timeout:10000})
    }
}