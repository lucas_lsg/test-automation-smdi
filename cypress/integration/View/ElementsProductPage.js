export default class ElementsProductPage {
   
    getSelectMTMPage(mtm){
        return cy.get('select[class="form-control pg-selector"]',{timeout:10000}).contains(mtm)
    }

    getButtonMTMModal(mtm){
        return cy.get('label[role="button"]',{timeout:10000}).contains(mtm)
    }

}