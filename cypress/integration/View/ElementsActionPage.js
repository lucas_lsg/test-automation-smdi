export default class ElementsActionPage {

    getButtonNewAction(){
        return cy.get('button[class="btn btn--primary"]',{timeout:10000}).contains('New Action')
    }
    getSearchAction(){
        return cy.get('#name',{timeout:10000})
    }
    getActionModalTitle(){
        return cy.get('.modal-title',{timeout:10000}).contains('Action Creation')
    } 
    getSelectNameModal(){
        return cy.get('#nameAction',{timeout:10000})
    }
}