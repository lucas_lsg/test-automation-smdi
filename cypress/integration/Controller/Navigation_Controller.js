import ElementsNavigationPage from '../View/ElementsNavigationPage.js'

let elements_NavigationPage = new ElementsNavigationPage;

export default class Navigation_Controller{

    goToElementsOperation(){
        cy.wait(1000)
        elements_NavigationPage.getCard_NavigationPage()
        cy.wait(1000)
        elements_NavigationPage.getCard_Simulation().click()
        cy.wait(1000)
        elements_NavigationPage.getButtonElements().click()
        cy.wait(1000)
        elements_NavigationPage.getButtonOperation().click()
        cy.wait(1000)
    }
    goToElementsLocation(){
        cy.wait(1000)
        elements_NavigationPage.getCard_NavigationPage()
        cy.wait(1000)
        elements_NavigationPage.getCard_Simulation().click()
        cy.wait(1000)
        elements_NavigationPage.getButtonElements().click()
        cy.wait(1000)
        elements_NavigationPage.getButtonLocation().click()
        cy.wait(1000)
    }
    goToElementsAction(){
        cy.wait(1000)
        elements_NavigationPage.getCard_NavigationPage()
        cy.wait(1000)
        elements_NavigationPage.getCard_Simulation().click()
        cy.wait(1000)
        elements_NavigationPage.getButtonElements().click()
        cy.wait(1000)
        elements_NavigationPage.getButtonAction().click()
        cy.wait(1000)
    }
    goToElementsOperationType(){
        cy.wait(1000)
        elements_NavigationPage.getCard_NavigationPage()
        cy.wait(1000)
        elements_NavigationPage.getCard_Simulation().click()
        cy.wait(1000)
        elements_NavigationPage.getButtonElements().click()
        cy.wait(1000)
        elements_NavigationPage.getButtonOperationType().click()
        cy.wait(1000)
    }
    goToElementsProduct(){
        cy.wait(1000)
        elements_NavigationPage.getCard_NavigationPage()
        cy.wait(1000)
        elements_NavigationPage.getCard_Simulation().click()
        cy.wait(1000)
        elements_NavigationPage.getButtonElements().click()
        cy.wait(1000)
        elements_NavigationPage.getButtonProduct().click()
        cy.wait(1000)
    }

    goToElementsGroup(){
        cy.wait(1000)
        elements_NavigationPage.getCard_NavigationPage()
        cy.wait(1000)
        elements_NavigationPage.getCard_Simulation().click()
        cy.wait(1000)
        elements_NavigationPage.getButtonElements().click()
        cy.wait(1000)
        elements_NavigationPage.getButtonGroup().click()
        cy.wait(1000)
    }
}