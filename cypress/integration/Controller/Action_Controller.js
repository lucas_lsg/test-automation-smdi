import ElementsActionPage from '../View/ElementsActionPage.js'
import ElementsGeneric from '../View/ElementsGeneric.js'

let elements_Generic = new ElementsGeneric;
let elementsActionPage = new ElementsActionPage;

export default class Action_Controller{
    
    createAction(name){
        elementsActionPage.getButtonNewAction().click({force:true})
        cy.wait(3000)       
        elementsActionPage.getSelectNameModal().type(name)
        elements_Generic.getButton_Save().click()
        cy.checkToastSuccess()
    }
    editAction(name){  
        elementsActionPage.getSearchAction().type(name)
        cy.wait(3000)
        elements_Generic.getButtonEdit().click()
        cy.wait(3000)
        elementsActionPage.getSelectNameModal().click()
        elementsActionPage.getSelectNameModal().type(' EDITED')
        elements_Generic.getButton_Save().click({force:true})
        cy.checkToastSuccess()
    }
    deleteAction(name){
        elementsActionPage.getSearchAction().clear()
        elementsActionPage.getSearchAction().type(name + ' EDITED')
        cy.wait(3000)
        elements_Generic.getButtonDelete().click({force:true})
        elements_Generic.getButton_Yes().click()
        cy.checkToastSuccess()
    }
}