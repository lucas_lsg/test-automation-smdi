import ElementsOperationPage from '../View/ElementsOperationPage.js'
import ElementsGeneric from '../View/ElementsGeneric.js'

let elements_Generic = new ElementsGeneric;
let elementsOperationPage = new ElementsOperationPage;


export default class Operation_Controller{

    checkElements(){
        elementsOperationPage.getButtonNewOperation()
        elements_Generic.getSearch()
    }
    createOperation(location, action,type, operation){
        this.checkElements()
        cy.wait(5000)
        elementsOperationPage.getButtonNewOperation().click({force:true})
        cy.wait(5000)
        elementsOperationPage.getSelectLocationModal().click()        
        elementsOperationPage.getSelectLocationModal().type(location)
        elements_Generic.getFilterResult().contains(location).click()
        cy.wait(2000)
        elementsOperationPage.getSelectActionModal().click()
        elementsOperationPage.getSelectActionModal().type(action)
        elements_Generic.getFilterResult().contains(action).click()
        cy.wait(2000)
        elementsOperationPage.getSelectTypeModal().click()
        elementsOperationPage.getSelectTypeModal().type(type)
        elements_Generic.getFilterResult().contains(type).click()
        cy.wait(2000)
        elementsOperationPage.getInputOperationModal().click()
        elementsOperationPage.getInputOperationModal().type(operation)

        elements_Generic.getButton_Save().click()
        cy.checkToastSuccess()
    }
    editOperation(operation){
        cy.wait(5000)
        elements_Generic.getSearch().type(operation)
        cy.wait(3000)
        elements_Generic.getButtonEdit().click()
        cy.wait(3000)
        elementsOperationPage.getInputOperationModal().type(' EDITED')
        elements_Generic.getButton_Save().click({force:true})
        cy.checkToastSuccess()
    }
    deleteOperation(operation){
        cy.wait(5000)
        elements_Generic.getSearch().clear()
        elements_Generic.getSearch().type(operation +' EDITED')
        cy.wait(3000)
        elements_Generic.getButtonDelete().click({force:true})
        elements_Generic.getButton_Yes().click()
        cy.checkToastSuccess()
    }
}