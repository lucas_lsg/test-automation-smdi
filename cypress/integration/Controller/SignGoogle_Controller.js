import ElementsGeneric from '../View/ElementsGeneric.js'
let elements_Generic = new ElementsGeneric;

export default class SignGoogle_Controller{

    signGooglePage(){
        cy.visit('/')
        if(elements_Generic.getHomeCardSignGoogle()){
            elements_Generic.getButtonSignGoogle().click()
            cy.wait(15000)
        }
        cy.wait(1000)
    }
}