import ElementsGroupPage from '../View/ElementsGroupPage.js'
import ElementsGeneric from '../View/ElementsGeneric.js'

let elements_Generic = new ElementsGeneric;
let elementsGroupPage = new ElementsGroupPage;


export default class Group_Controller{

    checkElements(){
        elementsGroupPage.getButtonNewGroup()
        elementsGroupPage.getSearchGroupPage()
    }
    createGroup(name, operation){
        this.checkElements()
        cy.wait(5000)
        elementsGroupPage.getButtonNewGroup().click({force:true})
        cy.wait(5000)
        elementsGroupPage.getInputNameModal().click()        
        elementsGroupPage.getInputNameModal().type(name)
        cy.wait(2000)        
        elementsGroupPage.getCheckBoxRealModal().click()
        cy.wait(2000)
        elementsGroupPage.getSelectOperationModal().click()
        elementsGroupPage.getSelectOperationModal().type(operation)
        elements_Generic.getFilterResult().contains(operation).click()
        cy.wait(2000)
        elementsGroupPage.getButtonAddOperation().click()
        cy.wait(2000)
        elements_Generic.getButton_Save().click()
        cy.checkToastSuccess()
    }
    editGroup(name){
        cy.wait(5000)
        elementsGroupPage.getSearchGroupPage().clear()
        elementsGroupPage.getSearchGroupPage().type(name)
        cy.wait(3000)
        elements_Generic.getButtonEdit().click()
        cy.wait(3000)
        elementsGroupPage.getInputNameModal().type(' EDITED')
        elements_Generic.getButton_Save().click({force:true})
        cy.checkToastSuccess()
    }
    deleteGroup(name){
        cy.wait(5000)
        elementsGroupPage.getSearchGroupPage().clear()
        elementsGroupPage.getSearchGroupPage().type(name +' EDITED')
        cy.wait(3000)
        elements_Generic.getButtonDelete().click({force:true})
        elements_Generic.getButton_Yes().click()
        cy.checkToastSuccess()
    }
}