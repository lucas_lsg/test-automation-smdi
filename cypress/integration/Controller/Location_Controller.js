import ElementsLocationPage from '../View/ElementsLocationPage.js'
import ElementsGeneric from '../View/ElementsGeneric.js'

let elements_Generic = new ElementsGeneric;
let elementsLocationPage = new ElementsLocationPage;

export default class Location_Controller{
    
    createLocation(name){ 
        elementsLocationPage.getButtonNewLocation().click({force:true})
        cy.wait(3000)       
        elementsLocationPage.getSelectNameModal().type(name)
        elements_Generic.getButton_Save().click()
        cy.checkToastSuccess()
    }
    editLocation(name){
        elementsLocationPage.getSearchLocation().type(name)
        cy.wait(3000)
        elements_Generic.getButtonEdit().click()
        cy.wait(3000)
        elementsLocationPage.getSelectNameModal().click()
        elementsLocationPage.getSelectNameModal().type(' EDITED')
        elements_Generic.getButton_Save().click({force:true})
        cy.checkToastSuccess()
    }
    deleteLocation(name){
        elementsLocationPage.getSearchLocation().clear()
        elementsLocationPage.getSearchLocation().type(name + ' EDITED')
        cy.wait(3000)
        elements_Generic.getButtonDelete().click({force:true})
        elements_Generic.getButton_Yes().click()
        cy.checkToastSuccess()
    }
}