import ElementsOperationTypePage from '../View/ElementsOperationTypePage.js'
import ElementsGeneric from '../View/ElementsGeneric.js'

let elements_Generic = new ElementsGeneric;
let elementsOperationTypePage = new ElementsOperationTypePage;


export default class OperationType_Controller{

    checkElements(){
        elementsOperationTypePage.getButtonNewOperationType()
        elements_Generic.getSearch()
    }
    createOperationType(type, complexity,description, CT){
        this.checkElements()
        cy.wait(5000)
        elementsOperationTypePage.getButtonNewOperationType().click({ force: true })
        cy.wait(3000)        
        elementsOperationTypePage.getInputTypeModal().type(type)
        elementsOperationTypePage.getInputComplexityModal().type(complexity)
        elementsOperationTypePage.getInputDescriptionModal().type(description)
        elementsOperationTypePage.getInputCTModal().type(CT)
        elements_Generic.getButton_Save().click()
        cy.checkToastSuccess()
    }
    editOperationType(type){
        cy.wait(5000)
        elements_Generic.getSearch().click()
        elements_Generic.getSearch().type(type)
        cy.wait(3000)
        elements_Generic.getButtonEdit().click()
        cy.wait(3000)
        elementsOperationTypePage.getInputTypeModal().type(' EDITED')
        elements_Generic.getButton_Save().click({ force: true })
        cy.checkToastSuccess()
    }
    deleteOperationType(type){
        cy.wait(5000)
        elements_Generic.getSearch().clear()
        elements_Generic.getSearch().type(type+ ' EDITED')
        cy.wait(3000)
        elements_Generic.getButtonDelete().click({ force: true })
        elements_Generic.getButton_Yes().click()
        cy.checkToastSuccess()
    }
}