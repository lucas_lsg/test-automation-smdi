import ElementsProductPage from '../View/ElementsProductPage.js'
import ElementsGeneric from '../View/ElementsGeneric.js'

let elements_Generic = new ElementsGeneric;
let elementsProductPage = new ElementsProductPage;


export default class Product_Controller{

    checkElements(){
        elements_Generic.getSearch()
    }
    
    editProduct(product){
        cy.wait(5000)
        elements_Generic.getSearch().clear()
        elements_Generic.getSearch().type(product)
        cy.wait(3000)
        elementsProductPage.getSelectMTMPage(" YES ")
        elements_Generic.getButtonEdit().click()
        cy.wait(3000)
        elementsProductPage.getButtonMTMModal('No')
        elements_Generic.getButton_Save().click({force:true})
        elements_Generic.getSearch().clear()
        elements_Generic.getSearch().type(product)
        cy.wait(3000)
        elementsProductPage.getSelectMTMPage(" NO ")
        elements_Generic.getButtonEdit().click()
        cy.wait(3000)
        elementsProductPage.getButtonMTMModal('Yes')
        elements_Generic.getButton_Save().click({force:true})
        cy.checkToastSuccess()
    }
   
}